const constants = require('./../constants/constants');
const fs = require('fs');
const path = require('path');

class Score {
    constructor() {
        this.filePath = path.join(__dirname, './../../dist/' + constants.SCORE_FILE_NAME);
    }

    addRecord( user ) {
        fs.appendFile( this.filePath, user.toString().toLowerCase() + ',',{encoding: "utf-8"}, (err) => {
            if (err) throw err;
        });
    }

    displayStat() {
        if( this.checkFileExists() ) {
            let content = fs.readFileSync(this.filePath, {flag:'r', encoding: "utf-8"}).toString();
            let result = [];

            if(content.length > 0) {
                content = content.split(',');

                for ( let i = 0; i < content.length - 1; i++ ) {
                    if( content[i] === '' ) {
                        continue;
                    }

                    if ( result[ content[i] ] !== undefined ) {
                        result[ content[i] ]++;
                    } else {
                        result[ content[i] ] = 1;
                    }
                }

                for ( let key in result ) {
                    console.log( key + " = " + result[ key ] );
                }
            } else {
                console.log( constants.SCORE_NO_RECORDS );
            }
        } else {
            console.log( constants.SCORE_NO_RECORDS );
        }
    }

    checkFileExists() {
        let flag = true;
        try{
            fs.accessSync( this.filePath, fs.constants.F_OK );
        }catch(e){
            flag = false;
        }
        return flag;
    }
}

module.exports = Score;