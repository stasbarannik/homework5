const constants = require('./../constants/constants');

class Player {
    constructor( name, marker ) {
        this.name = name;
        this.marker = marker;
        this.winner = false;
    }

    win() {
        this.winner = true;
    }
}

module.exports = Player;