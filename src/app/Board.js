const constants = require('./../constants/constants');
const Cell = require('./Cell');

class Board {
    constructor( dimensions ) {
        this.dimensions = dimensions;
        this.init();
    }

    init() {
        this.boardMatrix = [];

        for( let i = 0; i < this.dimensions; i++ ) {
            this.boardMatrix[i] = [];
            for( let j = 0; j < this.dimensions; j++ ) {
                this.boardMatrix[i][j] = new Cell();
            }
        }
    }

    display() {
        let row = '',
            border = this.getHorizontalBorder();

        // Drawing legend
        console.log( this.getHorizontalLegend() );

        for( let i = 0; i < this.dimensions; i++ ) {
            row = constants.BOARD_SEPARATORS.verticalIndent;

            for( let j = 0; j < this.dimensions; j++ ) {
                row += this.boardMatrix[i][j].getMark();

                if( j < this.dimensions - 1 ) {
                    row += constants.BOARD_SEPARATORS.vertical;
                }
            }

            // Drawing legend
            row += constants.BOARD_SEPARATORS.verticalIndent + (i + 1);

            console.log( row );

            if( i < this.dimensions - 1 ) {
                console.log( border );
            }
        }

    }

    getHorizontalBorder() {
        let output = '';
        for( let i = 0; i < this.dimensions; i++ ) {
            output += constants.BOARD_SEPARATORS.horizontal;
        }
        return output;
    }

    getHorizontalLegend() {
        let output = constants.BOARD_SEPARATORS.verticalIndent;
        for( let i = 0; i < this.dimensions; i++ ) {
            output += (i + 1);

            if( i < this.dimensions - 1 ) {
                output += constants.BOARD_SEPARATORS.verticalLegend;
            }
        }
        return output;
    }

    fillCell( x, y, playerID, playerMarker ) {
        if( this.boardMatrix[y][x].getPlayer() !== false ) {
            return false;
        }
        this.boardMatrix[y][x].setMark(playerID, playerMarker);
        return true;
    }

    getCell( x, y ) {
        return this.boardMatrix[y][x];
    }


}

module.exports = Board;
