const constants = require('./../constants/constants');
const readline = require('readline');
const Player = require('./Player');
const Board = require('./Board');
const Score = require('./Score');

class Game {
    constructor() {
        if ( process.argv.length > 2 && process.argv[2] === 'score' ) { //Run the game with score param to see the score
            let score = new Score();
            score.displayStat();
        } else {
            this.start();
        }
    }

    async start() {
        this.board = new Board( constants.BOARD_DIMENSION );
        await this.initPlayers();
        await this.loop();
    }

    async initPlayers() {
        this.players = [];
        this.turn = 0;

        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        for( let i = 0; i < constants.PLAYERS_QTY; i++ ) {
            console.log( constants.PLAYER + ( i + 1 )  );
            let name = await this.ask( constants.ASK_NAME_MSG, 20 );
            let marker = await this.ask( constants.ASK_MARK_MSG, 1 );

            await this.players.push( await new Player(name, marker) );
        }

        this.rl.close();
    }

    clear() {
        console.clear();
    }

    async makeMove( playerIndex ) {
        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        console.log( constants.MAKE_MOVE_MESSAGE + this.players[ playerIndex ].name );
        let x = await this.ask( this.players[ playerIndex ].name + constants.ASK_COORD_X_MSG , 1, true );
        let y = await this.ask( this.players[ playerIndex ].name + constants.ASK_COORD_Y_MSG , 1, true );

        this.rl.close();

        let response = this.board.fillCell( x-1, y-1, playerIndex, this.players[ playerIndex ].marker ); // Decrease coords because the first cell index is 0 instead of 1

        if( response ) {
            this.turn++;
            this.turn = this.turn === constants.PLAYERS_QTY ? 0 : this.turn;
        } else {
            console.log( constants.VALIDATION_COORDS_ALREADY_MARKED );
            await this.makeMove( playerIndex );
        }
    }

    haveWinner() {
        let verticalWinner = false;
        let horizontalWinner = false;
        let diagonal1Winner = false;
        let diagonal2Winner = false;


        for( let i = 0; i < constants.BOARD_DIMENSION; i++ ) {
            verticalWinner = this.board.getCell(i, 0).getPlayer();
            horizontalWinner = this.board.getCell(0, i).getPlayer();

            // Checking of vertical lines
            for( let j = 0; j < constants.BOARD_DIMENSION; j++ ) {
                if( verticalWinner !== false &&
                    verticalWinner !== this.board.getCell(i, j).getPlayer()) {
                    verticalWinner = false;
                    break;
                }
            }

            if( verticalWinner !== false ) {
                return verticalWinner;
            }

            // Checking of horizontal lines
            for( let j = 0; j < constants.BOARD_DIMENSION; j++ ) {
                if( horizontalWinner !== false &&
                    horizontalWinner !== this.board.getCell(j, i).getPlayer()) {
                    horizontalWinner = false;
                    break;
                }
            }

            if( horizontalWinner !== false ) {
                return horizontalWinner;
            }
        }

        // Checking of diagonal lines
        diagonal1Winner =  this.board.getCell(0, 0).getPlayer();
        for( let i = 1; i < constants.BOARD_DIMENSION; i++ ) {

            if( diagonal1Winner !== false &&
                diagonal1Winner !== this.board.getCell(i, i).getPlayer()) {
                diagonal1Winner = false;
                break;
            }
        }

        if( diagonal1Winner !== false ) {
            return diagonal1Winner;
        }

        diagonal2Winner =  this.board.getCell(0, constants.BOARD_DIMENSION - 1 ).getPlayer();
        for( let i = 1; i < constants.BOARD_DIMENSION; i++ ) {

            if( diagonal2Winner !== false &&
                diagonal2Winner !== this.board.getCell(i, constants.BOARD_DIMENSION - 1 - i).getPlayer()) {
                diagonal2Winner = false;
                break;
            }
        }

        if( diagonal2Winner !== false ) {
            return diagonal2Winner;
        }

        return false;
    }

    async loop() {
        this.clear();
        this.board.display();

        let winner;
        while ( ( winner = this.haveWinner() ) === false ) {
            await this.makeMove( this.turn );
            this.clear();
            this.board.display();
        }

        console.log(this.players[winner].name + constants.WINNER_MSG);

        let score = new Score();
        score.addRecord( this.players[winner].name );
    }

    async ask(q, limit, isCoords = false) {
        return new Promise((resolve, reject) => {
            this.rl.question(q, (data) => {
                if( data.length === 0 || data.length > limit ) {
                    console.log( constants.VALIDATION_ERROR_MSG );
                    resolve( this.ask( q, limit, isCoords ) );
                } else {
                    if( isCoords && ! this.validateCoord( data ) ) {
                        console.log( constants.VALIDATION_COORDS_ERROR_MSG + constants.BOARD_DIMENSION );
                        resolve( this.ask( q, limit, isCoords ) );
                    } else {
                        resolve( data );
                    }
                }
            });
        });
    }

    validateCoord( x ) {
        return Number.isInteger( parseInt(x) ) && 0 < parseInt(x) <= constants.BOARD_DIMENSION;
    }
}

module.exports = Game;