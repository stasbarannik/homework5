const constants = require('./../constants/constants');

class Cell {
    constructor() {
        this.value = constants.EMPTY_CELL;
        this.playerID = false;
    }

    getMark() {
        return this.value;
    }

    getPlayer() {
        return this.playerID;
    }

    setMark( playerID, marker ) {
        this.value = marker;
        this.playerID = playerID;
    }
}

module.exports = Cell;