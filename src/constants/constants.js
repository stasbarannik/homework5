module.exports = Object.freeze({
    ASK_NAME_MSG: 'Enter your name: ',
    ASK_MARK_MSG: 'Enter your marker (usually X or 0): ',
    MAKE_MOVE_MESSAGE: 'Turn of Player ',
    ASK_COORD_X_MSG: ', enter X coordinate: ',
    ASK_COORD_Y_MSG: ', enter Y coordinate: ',
    WINNER_MSG: ' has won the game!',
    PLAYER: 'Player ',

    VALIDATION_ERROR_MSG: 'Validation error',
    VALIDATION_COORDS_ERROR_MSG: 'Validation error. Please enter a number less than ',
    VALIDATION_COORDS_ALREADY_MARKED: 'You can\'t overwrite existing cells, please enter another coordinates',

    EMPTY_CELL: " ",
    BOARD_SEPARATORS: {
        horizontal: '-----',
        vertical:   '  |  ',
        verticalLegend:   '     ',
        verticalIndent:   ' ',
    },

    PLAYERS_QTY: 2,
    BOARD_DIMENSION: 3,

    SCORE_FILE_NAME: 'score.log',
    SCORE_NO_RECORDS: 'No records have been found',
});